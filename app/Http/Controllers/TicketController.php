<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =  Ticket::select('id','title','description','created_at')->where('status','1')->paginate(50);
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => 'Ticket listados',
        ], 200);
    }

    public function booking($id)
    {
        $ticket = Ticket::find($id,['id', 'status','title','description']);

        if(!isset($ticket->status)){
            $rps = response()->json([
                'status' => false,
                'message' => 'Ticket invalido',
            ], 404);
        }elseif($ticket->status=='0'){
            $rps = response()->json([
                'status' => false,
                'message' => 'Ticket no disponible',
            ], 401);
        }else{
            $ticket->update([
                'status' => '0' //reservado
            ]);
            $rps = response()->json([
                'status' => true,
                'message' => 'Ticket reservado',
            ], 202);
        }

        return $rps;
    }

    public function booking_me()
    {
        $data =  Ticket::select('id','title','description','created_at')
                    ->where('status','0')
                    ->where('user_id',Auth()->user()->id)
                    ->paginate(50);
        
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => 'Mis Ticket listados',
        ], 200);
    }
}
