<?php

namespace App\Observers;

use App\Models\Ticket;

class TicketObserver
{
    public function updating(Ticket $ticket)
    {   
        //antes de actualizr un ticket, si se esta marcando como reservado, se asocia al usuario en sesion
        //el \App::runningInConsole() se agrega para evitar al ejecutar los seed, descomentar para correr los test
        if(! \App::runningInConsole() && $ticket->status=='0'){
            $ticket->user_id = Auth()->user()->id;
        }
    }
}
