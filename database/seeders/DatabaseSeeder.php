<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\User::factory()->create([
            'email' => 'correo@correo.com',
            'password' => bcrypt('12345')
        ]);
        \App\Models\Ticket::factory(100)->create();
    }
}
