<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_users_can_login()
    {
        $user = User::factory()->create();

        $response = $this->post('/api/v1/login', [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $this->assertAuthenticated();
        $response->assertStatus(200)
        ->assertJsonStructure(['access_token','token_type','expires_at']);
    }

    public function test_users_can_not_login_password_invalid()
    {
        $user = User::factory()->create();

        $response = $this->post('/api/v1/login', [
            'email' => $user->email,
            'password' => 'password_malo'
        ]);

        $this->assertGuest();
        $response->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function test_users_can_register()
    {
        $response = $this->post('/api/v1/register', [
            'name' => 'Fulano',
            'lastname' => 'detal',
            'email' => time().'@correo.com',
            'password' => '12345',
        ]);

        $response->assertStatus(201)
        ->assertJsonStructure(['message']);
    }

    public function test_users_can_not_register_duplicate_email()
    {   
        $correo = time().'fulano@correo.com';
        User::factory()->create([
            'email' => $correo,
        ]);
        $response = $this->post('/api/v1/register', [
            'name' => 'Fulano',
            'lastname' => 'detal',
            'email' => $correo,
            'password' => '12345',
        ]);

        $response->assertStatus(302)
        ->assertJsonStructure(['error','message']);
    }
}
