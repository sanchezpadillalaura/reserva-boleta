<?php

namespace Tests\Feature;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Laravel\Passport\Passport;

class TicketTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_list_ticket()
    {
        Passport::actingAs($user = User::where('email', 'correo@correo.com')->first());
        $response = $this->get('/api/v1/tickets');
        $response->assertStatus(200)
        ->assertJsonStructure(['status','data','message']);
    }

    public function test_can_list_ticket_booking()
    {
        Passport::actingAs($user = User::where('email', 'correo@correo.com')->first());
        $response = $this->get('/api/v1/tickets/booking');
        $response->assertStatus(200)
        ->assertJsonStructure(['status','data','message']);
    }

    public function test_can_booking_ticket()
    {
        Passport::actingAs($user = User::where('email', 'correo@correo.com')->first());

        $ticket = Ticket::factory()->create();
        $response = $this->post('/api/v1/tickets/'.$ticket->id.'/booking');
        $response->assertStatus(202)
        ->assertJsonStructure(['status','message']);
        
        $t = Ticket::where('id',$ticket->id)
        ->where('user_id',$user->id)->get();
        
        $this->assertCount(1, $t);
    }

    public function test_can_not_booking_ticket_booked()
    {
        Passport::actingAs($user = User::where('email', 'correo@correo.com')->first());

        $ticket = Ticket::factory()->create(['status'=>'0']);
        $response = $this->post('/api/v1/tickets/'.$ticket->id.'/booking');
        $response->assertStatus(401)
        ->assertJsonStructure(['status','message']);
    }

    public function test_can_not_booking_ticket_false()
    {
        Passport::actingAs($user = User::where('email', 'correo@correo.com')->first());

        $response = $this->post('/api/v1/tickets/FALSE/booking');
        $response->assertStatus(404)
        ->assertJsonStructure(['status','message']);
    }
}
